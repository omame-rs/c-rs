cmake_minimum_required(VERSION 3.20)

add_subdirectory(graphics)
add_subdirectory(libjs)
add_subdirectory(psf)
add_subdirectory(theme)
add_subdirectory(widget)
add_subdirectory(window)
