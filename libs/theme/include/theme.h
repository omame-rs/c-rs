#pragma once
#include "graphics.h"

namespace LibTheme {

enum ThemeColor {
  TH_WALLPAPER_COLOR,
  TH_WINDOW_COLOR,
  TH_TITLE_BAR_COLOR,
};

enum ThemeFont {
  TH_TITLE_BAR_FONT,
};

LibGraphics::Color getColor(ThemeColor key);
LibGraphics::Font *getFont(ThemeFont key);

void setColor(ThemeColor key, LibGraphics::Color color);
void setFont(ThemeFont key, LibGraphics::Font *font);

} // namespace LibTheme
