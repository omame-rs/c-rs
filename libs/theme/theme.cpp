#include "theme.h"
#include "graphics.h"
#include <map>

using namespace LibTheme;

std::map<ThemeColor, LibGraphics::Color> colorMap = {
    {TH_WALLPAPER_COLOR, {0, 120, 120}},
    {TH_WINDOW_COLOR, {184, 184, 184}},
    {TH_TITLE_BAR_COLOR, {0, 0, 120}},
};

std::map<ThemeFont, LibGraphics::Font *> fontMap;

LibGraphics::Color LibTheme::getColor(ThemeColor key) { return colorMap[key]; }
void LibTheme::setColor(ThemeColor key, LibGraphics::Color color) {
  colorMap[key] = color;
}

LibGraphics::Font *LibTheme::getFont(ThemeFont key) { return fontMap[key]; }
void LibTheme::setFont(ThemeFont key, LibGraphics::Font *font) {
  fontMap[key] = font;
}
