#include "winmanage.h"
#include "drivers/fb/fbdriver.h"
#include "drivers/fb/fbmanager.h"
#include "event/loop.h"
#include "graphics.h"
#include "log.h"
#include "window.h"
#include <theme.h>
#include <vector>
#include <widget.h>

#define LOG(a, ...) printk("LibWindow::Management: " a, ##__VA_ARGS__)

using namespace LibGraphics;
using namespace LibWindow;
using namespace LibWindow::Management;

typedef struct {
  Window *window;
  int x;
  int y;
} WindowData;

FBDriver *display = nullptr;
std::vector<WindowData *> windows;
int g_id = 0;

// TODO: theming engine should replace this
int titleBarFontSize = 14;
int titleBarPadding = 2;
int windowBorderSize = 2;

// management drawing code
Image image;
ImageView imageView(&image);

void winManageRunEvery() {
  Rectangle background({display->get_size().width, display->get_size().height},
                       LibTheme::getColor(LibTheme::TH_WALLPAPER_COLOR));

  image.size = background.getSize();
  image.pixels = background.paint();

  Font *titleFont = LibTheme::getFont(LibTheme::TH_TITLE_BAR_FONT);
  titleFont->setPixelSize(titleBarFontSize);

  for (WindowData *data : windows) {
    // draw window border
    Size windowSize = data->window->content->getSize();

    Text text(data->window->title, titleFont);

    int penY = windowBorderSize;
    penY += text.getSize().height + 2;
    penY += windowSize.height;
    penY += windowBorderSize;
    LibWidget::Border border({windowSize.width + windowBorderSize * 2, penY},
                             true);

    int penX = windowBorderSize;
    penY = windowBorderSize;
    LayeredObject windowBorder(&border);

    penY += 1;
    penX += 1;
    Rectangle bg({windowSize.width - 2, text.getSize().height},
                 LibTheme::getColor(LibTheme::TH_TITLE_BAR_COLOR));

    windowBorder.addChild(&bg, penX, penY);
    windowBorder.addChild(&text, penX + titleBarPadding, penY);
    penX -= 1;
    penY += bg.getSize().height + 1;
    windowBorder.addChild(data->window->content, penX, penY);

    Image window = objectToImage(&windowBorder);
    alphaBlend(image, window, data->x, data->y);
    delete window.pixels;
  }

  // use the ImageView to draw the final buffer
  objectToFB(&imageView, display, 0, 0);
}

void LibWindow::Management::start(int index) {
  display = fb_get_device(index);
  LOG("will run on display %p (%dx%d)", display, display->get_size().width,
      display->get_size().height);

  LOG("starting");
  event_loop_run_every(winManageRunEvery);
}

void LibWindow::Management::openWindow(Window *window) {
  window->id = g_id++;
  WindowData *data = new WindowData{window, 16, 16};
  windows.push_back(data);
}

void LibWindow::Management::closeWindow(Window *window) {
  // TODO: close window
  printk("TODO: close window %p", window);
}
