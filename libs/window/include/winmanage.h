#pragma once
#include <graphics.h>

namespace LibWindow {
class Window;
}

namespace LibWindow::Management {

void start(int index);
void openWindow(Window *window);
void closeWindow(Window *window);

} // namespace LibWindow::Management
