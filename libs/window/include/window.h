#pragma once
#include <graphics.h>
#include <winmanage.h>

namespace LibWindow {

class Window {
public:
  Window(LibGraphics::Object *content, const char *title);

  int id = -1;
  const char *title;
  LibGraphics::Object *content;
};

} // namespace LibWindow
