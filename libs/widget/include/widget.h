#pragma once

#include "graphics.h"
namespace LibWidget {

class Border : public LibGraphics::Object {
public:
  Border(LibGraphics::Size size, bool isWindow = false);

  LibGraphics::Size getSize();
  LibGraphics::Color *paint();

private:
  LibGraphics::Size m_size;
  bool m_isWindow;
};

} // namespace LibWidget
