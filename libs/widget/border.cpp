#include "graphics.h"
#include "theme.h"
#include "widget.h"

using namespace LibWidget;
using namespace LibGraphics;

Border::Border(Size size, bool isWindow) : m_size(size), m_isWindow(isWindow) {}

Size Border::getSize() { return m_size; }

Color *Border::paint() {
  Color *buffer = CALLOC_COLOR(m_size);

  Color windowColor = LibTheme::getColor(LibTheme::TH_WINDOW_COLOR);
  Color lighter1Color = {(uint8_t)(windowColor.r + 32),
                         (uint8_t)(windowColor.g + 32),
                         (uint8_t)(windowColor.b + 32)};
  Color lighter2Color = {(uint8_t)(windowColor.r + 64),
                         (uint8_t)(windowColor.g + 64),
                         (uint8_t)(windowColor.b + 64)};

  Color darkerColor = {(uint8_t)(windowColor.r - 64),
                       (uint8_t)(windowColor.g - 64),
                       (uint8_t)(windowColor.b - 64)};

  Color black = {0, 0, 0};

  Color outsideColor = m_isWindow ? lighter1Color : lighter2Color;
  Color insideColor = m_isWindow ? lighter2Color : lighter1Color;

  for (int i = 0; i < m_size.width * m_size.height; i++) {
    // fill the buffer with the window color
    buffer[i] = windowColor;
  }

  // top border
  for (int x = 0; x < m_size.width - 1; x++) {
    buffer[x] = outsideColor;
    if (x == 0 || x == m_size.width - 2)
      continue;
    buffer[m_size.width + x] = insideColor;
  }

  // left border
  for (int y = 1; y < m_size.height - 1; y++) {
    buffer[y * m_size.width] = outsideColor;
    if (y == 1 || y == m_size.height - 2)
      continue;
    buffer[y * m_size.width + 1] = insideColor;
  }

  // bottom border
  int idxOff = (m_size.height - 1) * m_size.width;
  int idxOffInner = (m_size.height - 2) * m_size.width;
  for (int x = 0; x < m_size.width; x++) {
    buffer[idxOff + x] = black;
    if (x == 0 || x == m_size.width - 2)
      continue;
    buffer[idxOffInner + x] = darkerColor;
  }

  // top border
  int x = m_size.width - 1;
  for (int y = 0; y < m_size.height - 1; y++) {
    buffer[y * m_size.width + x] = black;
    if (y == 0)
      continue;
    buffer[y * m_size.width + x - 1] = darkerColor;
  }

  return buffer;
}
