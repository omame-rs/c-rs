class GraphicsObject {
  constructor(ptr) {
    this.ptr = ptr;
  }

  get size() {
    const ptr = ccall(
      "libjs_graphics_object_getSize",
      "number",
      ["number"],
      [this.ptr]
    );

    const width = HEAP32[ptr >> 2];
    const height = HEAP32[(ptr >> 2) + 1];
    _free(ptr);
    return [width, height];
  }
}

class Font {
  constructor(ptr) {
    this.ptr = ptr;
  }

  setPixelSize(size) {
    ccall(
      "libjs_graphics_font_setPixelSize",
      null,
      ["number", "number"],
      [this.ptr, size]
    );
  }

  /**
   * @param {string} path
   * @param {number?} faceIndex
   * @returns
   */
  static loadFromFile(path, faceIndex) {
    if (!faceIndex) faceIndex = 0;
    const ptr = ccall(
      "libjs_graphics_font_loadFromFile",
      "number",
      ["string", "number"],
      [path, faceIndex]
    );
    return new Font(ptr);
  }

  /**
   * @param {Uint8Array} arr
   * @param {number?} faceIndex
   * @returns
   */
  static loadFromArray(arr, faceIndex) {
    if (!faceIndex) faceIndex = 0;
    const buffer = _malloc(arr.length);
    HEAPU8.set(arr, buffer);

    const ptr = ccall(
      "libjs_graphics_font_loadFromMemory",
      "number",
      ["number", "number", "number"],
      [buffer, arr.length, faceIndex]
    );

    _free(buffer);
    return new Font(ptr);
  }
}

libjs.graphics = {
  Size: (width, height) => [width, height],
  Color: (r, g, b, a) => [r, g, b, typeof a === "number" ? a : 255],
  Rectangle: (size, color) => {
    const ptr = ccall(
      "libjs_graphics_newRectangle",
      "number",
      ["number", "number", "number", "number", "number", "number"],
      [...size, ...color]
    );
    return new GraphicsObject(ptr);
  },
  Font,
  Text: (text, font) => {
    const ptr = ccall(
      "libjs_graphics_newText",
      "number",
      ["string", "number"],
      [text, font.ptr]
    );
    return new GraphicsObject(ptr);
  },
};
