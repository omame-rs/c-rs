#include "window.h"
#include "winmanage.h"
#include <cstring>

extern "C" LibWindow::Window *
libjs_window_newWindow(LibGraphics::Object *content, const char *title) {
  size_t len = strlen(title);
  char *outOfStackTitle = (char *)malloc(len + 1);
  memcpy(outOfStackTitle, title, len + 1);
  return new LibWindow::Window(content, outOfStackTitle);
}

extern "C" int libjs_window_getId(LibWindow::Window *window) {
  return window->id;
}

extern "C" void libjs_window_management_start(int index) {
  LibWindow::Management::start(index);
}

extern "C" void libjs_window_management_openWindow(LibWindow::Window *window) {
  LibWindow::Management::openWindow(window);
}

extern "C" void libjs_window_management_closeWindow(LibWindow::Window *window) {
  LibWindow::Management::closeWindow(window);
}
