class Window {
  constructor(content, title) {
    this.ptr = ccall(
      "libjs_window_newWindow",
      "number",
      ["number", "string"],
      [content.ptr, title]
    );
    this.title = title;
    this.content = content;
  }

  get id() {
    return ccall("libjs_window_getId", "number", ["number"], [this.ptr]);
  }
}

libjs.window = {
  Window,
  management: {
    start: (index) =>
      ccall("libjs_window_management_start", null, ["number"], [index]),
    openWindow: (window) =>
      ccall(
        "libjs_window_management_openWindow",
        null,
        ["number"],
        [window.ptr]
      ),
    closeWindow: (window) =>
      ccall(
        "libjs_window_management_closeWindow",
        null,
        ["number"],
        [window.ptr]
      ),
  },
};
