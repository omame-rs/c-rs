#include "cmdline.h"
#include "drivers/tty/ttymanager.h"
#include "error.h"
#include "panic.h"
#include <cstdlib>

extern "C" void libjs_print(char *string) {
  int desirableTTY = -1;
  char *desirableTTYString = get_cmdline_value("console");
  if (desirableTTYString != nullptr) {
    desirableTTY = atoi(desirableTTYString);
    if (tty_get_device(desirableTTY) == nullptr)
      desirableTTY = -1;
  }

  if (desirableTTY == -1) {
    for (int i = 0; i < tty_get_device_count(); i++) {
      tty_puts(tty_get_device(i), string);
    }
  } else {
    tty_puts(tty_get_device(desirableTTY), string);
  }
}

extern "C" void libjs_panic(char *reason) { panic(-ERR_LIBJS_PANIC, reason); }
