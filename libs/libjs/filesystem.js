class File {
  constructor(ptr) {
    this.ptr = ptr;
  }

  async read(n, offset) {
    const buffer = _malloc(n);

    const count = await ccall(
      "libjs_fs_read",
      "number",
      ["number", "number", "number", "number"],
      [this.ptr, buffer, n, offset],
      { async: true }
    );

    const ret = new Uint8Array(count);
    ret.set(HEAPU8.slice(buffer, buffer + count));

    _free(buffer);
    return ret;
  }

  /**
   * @param {Uint8Array} arr
   * @param {number} offset
   */
  async write(arr, offset) {
    const buffer = _malloc(arr.length);
    HEAPU8.set(arr, buffer);

    await ccall(
      "libjs_fs_write",
      null,
      ["number", "number", "number", "number"],
      [this.ptr, buffer, arr.length, offset],
      { async: true }
    );

    _free(buffer);
  }
}

libjs.fs = {
  mount: (device, filesystem) => {
    const char = ccall(
      "libjs_fs_mount",
      "number",
      ["string", "string"],
      [device, filesystem]
    );

    return String.fromCharCode(char);
  },
  open: (path, flags) => {
    const ptr = ccall(
      "libjs_fs_open",
      "number",
      ["string", "number"],
      [path, flags]
    );
    return new File(ptr);
  },

  OPEN_R: 1 << 0,
  OPEN_W: 1 << 1,
  OPEN_CREATE: 1 << 2,
};
