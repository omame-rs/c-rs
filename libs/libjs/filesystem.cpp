#include "vfs.h"
#include <cstddef>

extern "C" {
char libjs_fs_mount(const char *device, const char *filesystem) {
  return vfs_mount(device, filesystem);
}

VFSFile *libjs_fs_open(const char *path, int flags) {
  return vfs_open(path, flags);
}

size_t libjs_fs_read(VFSFile *file, uint8_t *buf, size_t n, size_t offset) {
  return file->read(buf, n, offset);
}

void libjs_fs_write(VFSFile *file, uint8_t *buf, size_t n, size_t offset) {
  file->write(buf, n, offset);
}
}
