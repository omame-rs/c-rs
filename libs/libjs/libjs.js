var libjs = {};

(() => {
  // includes go here

  #include "print.js"
  #include "filesystem.js"
  #include "graphics.js"
  #include "window.js"
})();
