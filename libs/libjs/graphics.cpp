#include <cstdint>
#include <graphics.h>

using namespace LibGraphics;

extern "C" {
// Object
int *libjs_graphics_object_getSize(Object *object) {
  int *sizeBuf = (int *)malloc(2 * sizeof(int));
  Size size = object->getSize();
  sizeBuf[0] = size.width;
  sizeBuf[1] = size.height;
  return sizeBuf;
}

// Rectangle
Rectangle *libjs_graphics_newRectangle(int width, int height, uint8_t r,
                                       uint8_t g, uint8_t b, uint8_t a) {
  return new Rectangle({width, height}, {r, g, b, a});
}

// Font
Font *libjs_graphics_font_loadFromFile(const char *path, int faceIndex) {
  return Font::loadFromFile(path, faceIndex);
}

Font *libjs_graphics_font_loadFromMemory(uint8_t *buf, size_t size,
                                         int faceIndex) {
  return Font::loadFromMemory(buf, size, faceIndex);
}

void libjs_graphics_font_setPixelSize(Font *font, int size) {
  font->setPixelSize(size);
}

// Text
Text *libjs_graphics_newText(const char *text, Font *font) {
  return new Text(text, font);
}
}
