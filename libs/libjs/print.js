libjs.print = cwrap("libjs_print", null, ["string"]);

let haveAborted = false;
Module.onAbort = (what) => {
  if (haveAborted) return;
  haveAborted = true;
  console.warn("ABORT CALLED", what);
  ccall("libjs_panic", null, ["string"], [`Aborted (${what})`]);
};
