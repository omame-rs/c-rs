#include "freetype/freetype.h"
#include "graphics_init.h"
#include "log.h"

using namespace LibGraphics::Internal;

FT_Library LibGraphics::Internal::library;
void LibGraphics::Internal::init() {
  int error = FT_Init_FreeType(&library);
  if (error) {
    printk("LibGraphics: something went wrong during FreeType init: %d", error);
    return;
  }
  printk("LibGraphics: initialized");
}
