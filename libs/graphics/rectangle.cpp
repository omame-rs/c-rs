#include "graphics.h"
#include <cstdlib>

using namespace LibGraphics;

Rectangle::Rectangle(Size size, Color color) : m_size(size), m_color(color) {}

Size Rectangle::getSize() { return m_size; }

Color *Rectangle::paint() {
  Color *buffer = CALLOC_COLOR(m_size);

  for (int i = 0; i < m_size.width * m_size.height; i++) {
    buffer[i] = m_color;
  }

  return buffer;
}
