#include "assert.h"
#include "graphics.h"
#include <cstring>
#include <ft2build.h>
#include FT_FREETYPE_H

using namespace LibGraphics;

#define RESIZE(face, x) (FT_MulFix(face->x, face->size->metrics.y_scale) >> 6)

Text::Text(const char *text, Font *font) : m_text(text), m_font(font) {
  int numChars = strlen(text);
  Image buffers[numChars];

  int finalWidth = 0;
  int height = 0;

  for (int i = 0; i < numChars; i++) {
    uint32_t glyphIndex = font->getGlyphIndex(text[i]);

    FT_GlyphSlot glyph = font->renderGlyph(glyphIndex);
    FT_Bitmap &bitmap = glyph->bitmap;

    ASSERT(bitmap.pixel_mode == FT_PIXEL_MODE_GRAY);

    if (height == 0) {
      height = RESIZE(glyph->face, bbox.yMax) - RESIZE(glyph->face, descender);
    }

    Size size = {(int)(glyph->advance.x >> 6), height};
    buffers[i] = {size, CALLOC_COLOR(size)};
    finalWidth += size.width;

    for (int y = 0; y < bitmap.rows; y++) {
      for (int x = 0; x < bitmap.width; x++) {
        int oldIdx = y * bitmap.width + x;
        uint8_t clr = bitmap.buffer[oldIdx];
        if (clr == 0)
          continue;
        int newIdx = (y + (height - glyph->bitmap_top +
                           RESIZE(glyph->face, descender))) *
                         size.width +
                     (x + glyph->bitmap_left);
        buffers[i].pixels[newIdx] = {255, 255, 255, clr};
      }
    }
  }

  ASSERT(height != 0);
  Size size = {finalWidth, height};
  Color *finalBuffer = CALLOC_COLOR(size);

  int penX = 0;
  for (int i = 0; i < numChars; i++) {
    for (int y = 0; y < buffers[i].size.height; y++) {
      for (int x = 0; x < buffers[i].size.width; x++) {
        int newX = x + penX;
        int idx = y * buffers[i].size.width + x;
        int newIdx = y * size.width + newX;
        finalBuffer[newIdx] = buffers[i].pixels[idx];
      }
    }

    penX += buffers[i].size.width;
    free(buffers[i].pixels);
  }

  m_buffer = new Image{size, finalBuffer};
}

Size Text::getSize() { return m_buffer->size; }
Color *Text::paint() { return m_buffer->pixels; }

Text::~Text() {
  delete m_buffer->pixels;
  delete m_buffer;
}
