#include "graphics.h"
#include <cstdlib>

void LibGraphics::objectToFB(Object *object, FBDriver *driver, int x, int y) {
  Color *image = object->paint();
  Size size = object->getSize();
  driver->write((uint8_t *)image, x, y, size.width, size.height);
  free(image);
}

LibGraphics::Image LibGraphics::objectToImage(Object *object) {
  Color *buf = object->paint();
  Size size = object->getSize();

  return {size, buf};
}

void LibGraphics::alphaBlend(Image dest, Image src, int x, int y) {
  // for simplicity's sake, this assumes that dest's alpha channel is 100% 0xFF

  for (int j = 0; j < src.size.height; j++) {
    for (int i = 0; i < src.size.width; i++) {
      int offI = i + x;
      int offJ = j + y;
      if (offI > dest.size.width || offJ > dest.size.height)
        continue;

      int idx = offJ * dest.size.width + offI;
      int srcIdx = j * src.size.width + i;
      int alpha = src.pixels[srcIdx].a;

#define BLEND_PIXEL(r)                                                         \
  dest.pixels[idx].r = (dest.pixels[idx].r * (255 - alpha) / 255) +            \
                       (src.pixels[srcIdx].r * alpha / 255);

      BLEND_PIXEL(r);
      BLEND_PIXEL(g);
      BLEND_PIXEL(b);
    }
  }
}
