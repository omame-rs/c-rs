#include "graphics.h"
#include "graphics_init.h"
#include "vfs.h"
#include <cstdint>
#include <cstdlib>
#include <ft2build.h>
#include FT_FREETYPE_H

using namespace LibGraphics;

#define FILE_READ_CHUNK 1024

Font::Font() {}

Font *Font::loadFromFile(const char *path, int faceIndex) {
  Font *font = new Font();

  // load file
  uint8_t *buffer = (uint8_t *)malloc(FILE_READ_CHUNK);
  size_t fileSize = 0;

  size_t read;
  VFSFile *file = vfs_open(path, OPEN_R);
  while ((read = file->read(buffer + fileSize, FILE_READ_CHUNK, fileSize)) ==
         FILE_READ_CHUNK) {
    fileSize += read;
    buffer = (uint8_t *)realloc(buffer, fileSize + FILE_READ_CHUNK);
  }
  fileSize += read;

  int error = FT_New_Memory_Face(Internal::library, buffer, fileSize, faceIndex,
                                 &font->m_face);
  free(buffer);

  if (error) {
    delete font;
    return nullptr;
  }

  return font;
}

Font *Font::loadFromMemory(uint8_t *buf, size_t size, int faceIndex) {
  Font *font = new Font();

  int error = FT_New_Memory_Face(Internal::library, buf, size, faceIndex,
                                 &font->m_face);

  if (error) {
    delete font;
    return nullptr;
  }

  return font;
}

uint32_t Font::getGlyphIndex(char c) { return FT_Get_Char_Index(m_face, c); }

void Font::setPixelSize(int size) { FT_Set_Pixel_Sizes(m_face, size, size); }

FT_GlyphSlot Font::renderGlyph(uint32_t glyphIndex) {
  FT_GlyphSlot glyph = m_face->glyph;
  FT_Load_Glyph(m_face, glyphIndex, FT_LOAD_DEFAULT);
  FT_Render_Glyph(glyph, FT_RENDER_MODE_NORMAL);

  return glyph;
}
