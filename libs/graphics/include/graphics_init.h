#pragma once

#include <ft2build.h>
#include FT_FREETYPE_H

namespace LibGraphics::Internal {

extern FT_Library library;
void init();

} // namespace LibGraphics::Internal
