#pragma once
#include "drivers/fb/fbdriver.h"
#include <ft2build.h>
#include <vector>
#include FT_FREETYPE_H
#include <cstdint>

#define CALLOC_COLOR(size)                                                     \
  (LibGraphics::Color *)(calloc(size.width * size.height, 4))

namespace LibGraphics {

typedef struct {
  int width;
  int height;
} Size;

typedef struct Color {
  uint8_t r;
  uint8_t g;
  uint8_t b;
  uint8_t a = 255;
} Color;

typedef struct {
  Size size;
  Color *pixels;
} Image;

class Object {
public:
  virtual Color *paint();
  virtual Size getSize();
};

class Rectangle : public Object {
public:
  Rectangle(Size size, Color color);
  Color *paint();
  Size getSize();

private:
  Color m_color;
  Size m_size;
};

class Font {
public:
  static Font *loadFromFile(const char *path, int faceIndex = 0);
  static Font *loadFromMemory(uint8_t *buf, size_t size, int faceIndex = 0);

  void setPixelSize(int size);
  uint32_t getGlyphIndex(char c);
  FT_GlyphSlot renderGlyph(uint32_t glyphIndex);

private:
  Font();
  FT_Face m_face;
};

class Text : public Object {
public:
  Text(const char *text, Font *font);
  ~Text();
  Color *paint();
  Size getSize();

private:
  const char *m_text;
  Font *m_font;
  // render once
  Image *m_buffer;
};

class LayeredObject : public Object {
public:
  typedef struct {
    Object *object;
    int x;
    int y;
  } Child;

  LayeredObject(Object *background);

  void addChild(Object *object, int x, int y);

  Size getSize();
  Color *paint();

private:
  Object *m_background;
  std::vector<Child> m_children;
};

class ImageView : public Object {
public:
  ImageView(Image *image);

  Size getSize();
  Color *paint();

private:
  Image *m_image;
};

void objectToFB(Object *object, FBDriver *driver, int x, int y);
void alphaBlend(Image dest, Image src, int x, int y);
Image objectToImage(Object *object);

} // namespace LibGraphics
