#include "graphics.h"

using namespace LibGraphics;

ImageView::ImageView(Image *image) : m_image(image) {}

Size ImageView::getSize() { return m_image->size; }
Color *ImageView::paint() { return m_image->pixels; }
