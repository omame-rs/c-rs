#include "graphics.h"

using namespace LibGraphics;

LayeredObject::LayeredObject(Object *background) : m_background(background) {}
Size LayeredObject::getSize() { return m_background->getSize(); }

void LayeredObject::addChild(Object *object, int x, int y) {
  m_children.push_back({object, x, y});
}

Color *LayeredObject::paint() {
  Color *buffer = m_background->paint();
  Image image = {m_background->getSize(), buffer};

  for (auto &child : m_children) {
    Color *childBuf = child.object->paint();
    Size childSize = child.object->getSize();
    Image childImage = {childSize, childBuf};

    alphaBlend(image, childImage, child.x, child.y);

    delete childBuf;
  }

  return buffer;
}
