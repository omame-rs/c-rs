#include "psf.h"
#include "log.h"
#include <cstdint>
#include <cstdio>
#include <cstdlib>

using namespace LibPSF;

#define PSF_FONT_MAGIC 0x864ab572

typedef struct {
  uint32_t magic;
  uint32_t version;
  uint32_t headerSize;
  uint32_t flags;
  uint32_t numOfGlyphs;
  uint32_t bytesPerGlyph;
  uint32_t height;
  uint32_t width;
} PSF_font_header;

Font::Font(uint8_t *data) : data(data) {
  PSF_font_header *header = (PSF_font_header *)data;
  if (header->magic != PSF_FONT_MAGIC) {
    printk("PSF: expected %#x, got %#x", PSF_FONT_MAGIC, header->magic);
    this->data = nullptr;
    return;
  }

  if (header->flags != 0) {
    printk("PSF: flags=%d unicode support is practically nonexistent",
           header->flags);
  }

  if (header->width != 8) {
    printk("PSF: fonts with a width of not 8 are likely to be borked youve "
           "been warned");
  }

  this->headerSize = header->headerSize;
  this->flags = header->flags;
  this->numOfGlyphs = header->numOfGlyphs;
  this->bytesPerGlyph = header->bytesPerGlyph;
  this->height = header->height;
  this->width = header->width;

  printk("PSF: loaded font");
}

uint8_t shifted(int i, int j) { return (i >> (7 - j)) & 1; }

uint8_t *Font::glyph_to_rgba(char glyph, RGBA bg, RGBA fg) {
  if (this->data == nullptr)
    return nullptr;
  uint8_t *rgba = (uint8_t *)malloc(this->width * this->height * 4);
  // get glyph data
  uint8_t *glyph_data = &this->data[glyph * bytesPerGlyph + headerSize];

  for (int j = 0; j < height; j++) {
    for (int i = 0; i < width; i++) {
      RGBA color = shifted(glyph_data[j], i) == 1 ? fg : bg;

      int buf_idx = (j * width + i) * 4;
      rgba[buf_idx] = color.r;
      rgba[buf_idx + 1] = color.g;
      rgba[buf_idx + 2] = color.b;
      rgba[buf_idx + 3] = color.a;
    }
  }
  return rgba;
}
