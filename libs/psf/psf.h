#pragma once
#include <cstdint>

namespace LibPSF {

typedef struct {
  uint8_t r;
  uint8_t g;
  uint8_t b;
  uint8_t a;
} RGBA;

class Font {
public:
  Font(uint8_t *data);

  uint32_t flags;
  uint32_t numOfGlyphs;
  uint32_t bytesPerGlyph;
  uint32_t height;
  uint32_t width;

  uint8_t *glyph_to_rgba(char glyph, RGBA bg, RGBA fg);

private:
  uint32_t headerSize;
  uint8_t *data;
};

} // namespace LibPSF
