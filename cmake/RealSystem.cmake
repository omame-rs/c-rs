function(rs_add_resource TARGET FILE_PATH)
  add_custom_command(
    OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/generated/${FILE_PATH}.cpp ${CMAKE_CURRENT_BINARY_DIR}/generated/${FILE_PATH}.h
    COMMAND ${CMAKE_SOURCE_DIR}/tools/rsrc_to_cpp.py
      ${CMAKE_CURRENT_SOURCE_DIR}
      ${CMAKE_CURRENT_SOURCE_DIR}/${FILE_PATH}
      ${CMAKE_CURRENT_BINARY_DIR}/generated/${FILE_PATH}.cpp
    DEPENDS ${FILE_PATH} ${CMAKE_SOURCE_DIR}/tools/rsrc_to_cpp.py
  )
  target_sources(${TARGET} PRIVATE ${CMAKE_CURRENT_BINARY_DIR}/generated/${FILE_PATH}.cpp)
endfunction()

function(target_preprocess TARGET SRC OUT)
  add_custom_command(
    OUTPUT ${OUT}
    COMMAND gcc -E -P -w -x c ${SRC} -o ${OUT}
    WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
    MAIN_DEPENDENCY ${SRC}
  )
  get_filename_component(FAKE_TGT "${OUT}" NAME_WE)
  add_custom_target(tgt_${FAKE_TGT} DEPENDS ${OUT})
  add_dependencies(${TARGET} tgt_${FAKE_TGT})
endfunction()

set(EXPORTED_FUNCTIONS "_main" CACHE STRING "" FORCE)
function(export_symbol SYMBOL)
  set(EXPORTED_FUNCTIONS "${EXPORTED_FUNCTIONS},_${SYMBOL}" CACHE STRING "" FORCE)
endfunction()

function(rs_dir_to_json HTTP_PATH LOCAL_PATH)
  execute_process(
    COMMAND python ${CMAKE_SOURCE_DIR}/tools/dir_json.py "${HTTP_PATH}" "${LOCAL_PATH}"
  )
endfunction()
