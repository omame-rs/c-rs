#include "cmdline.h"
#include "log.h"
#include <cstdlib>
#include <cstring>
#include <map>
#ifdef __EMSCRIPTEN__
#include <emscripten.h>
#endif

std::map<char *, char *> cmdline_map;

void load_cmdline() {
  char *cmdline = (char *)EM_ASM_PTR({
    const cmdline = window.KERNEL_APPEND || "";
    const binCmdline = new TextEncoder().encode(cmdline);
    const ptr = _malloc(binCmdline.length + 1);
    HEAPU8.set(binCmdline, ptr);
    HEAPU8[ptr + binCmdline.length] = 0;
    return ptr;
  });

  char *p = cmdline;

  int key_len = 0;
  int key_off = 0;
  int val_len = 0;
  int val_off = 0;

  while (*(++p) != 0) {
    if (*p == '=') {
      key_len = (p - cmdline) - key_off;
      val_off = (p - cmdline) + 1;
    }

    if (*p == ' ') {
      val_len = (p - cmdline) - val_off;
      // push key/val pair onto map
      char *key = (char *)malloc(key_len + 1);
      char *val = (char *)malloc(val_len + 1);
      memcpy(key, cmdline + key_off, key_len);
      memcpy(val, cmdline + val_off, val_len);
      key[key_len] = 0;
      val[key_len] = 0;
      cmdline_map[key] = val;

      // set new key offset
      if (*(p + 1) != 0)
        key_off = (p - cmdline) + 1;
    }
  }

  // do final iteration of the p loop
  {
    val_len = (p - cmdline) + val_off;
    // push key/val pair onto map
    char *key = (char *)malloc(key_len + 1);
    char *val = (char *)malloc(val_len + 1);
    memcpy(key, cmdline + key_off, key_len);
    memcpy(val, cmdline + val_off, val_len);
    key[key_len] = 0;
    val[val_len] = 0;
    cmdline_map[key] = val;
  }

  free(cmdline);
}

char *get_cmdline_value(const char *key) {
  for (const auto &[our_key, value] : cmdline_map) {
    if (strcmp(key, our_key) == 0) {
      return value;
    }
  }

  return nullptr;
}
