#pragma once

typedef void (*EventLoopCallback)();

void event_loop_register();

/**
    Run on every loop iteration.
*/
void event_loop_run_every(EventLoopCallback callback);

/**
    Run on the next loop iteration.
*/
void event_loop_run_once(EventLoopCallback callback);

void event_loop_stop_every(EventLoopCallback callback);
