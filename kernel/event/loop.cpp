#include "event/loop.h"
#include <pthread.h>
#include <vector>
#ifdef __EMSCRIPTEN__
#include "log.h"
#include <emscripten.h>
#endif

std::vector<EventLoopCallback> m_callbacks_every;
std::vector<EventLoopCallback> m_callbacks_once;
pthread_mutex_t m_callback_mutex = PTHREAD_MUTEX_INITIALIZER;

void _event_main_loop() {
  pthread_mutex_lock(&m_callback_mutex);
  for (EventLoopCallback callback : m_callbacks_every) {
    callback();
  }

  for (EventLoopCallback callback : m_callbacks_once) {
    callback();
  }
  m_callbacks_once.clear();
  pthread_mutex_unlock(&m_callback_mutex);
}

void event_loop_register() {
#ifdef __EMSCRIPTEN__
  printk("EventLoop: main loop set");
  emscripten_set_main_loop(_event_main_loop, -1, false);
#endif
}

void event_loop_run_every(EventLoopCallback callback) {
  pthread_mutex_lock(&m_callback_mutex);
  m_callbacks_every.push_back(callback);
  pthread_mutex_unlock(&m_callback_mutex);
}

void event_loop_run_once(EventLoopCallback callback) {
  pthread_mutex_lock(&m_callback_mutex);
  m_callbacks_once.push_back(callback);
  pthread_mutex_unlock(&m_callback_mutex);
}
