#pragma once

#define HAS_FLAG(x, flag) ((x & flag) == flag)
#define HAS_ANY_FLAG(x, flags) ((x & flags) != 0)
