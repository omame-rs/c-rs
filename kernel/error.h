#pragma once

#define ERR_NOT_EXIST 1
#define ERR_EXIST 2
#define ERR_INVALID 10
#define ERR_NOT_IMPL 11
#define ERR_ASSERT 12

#define ERR_LIBJS_PANIC 100

#define IS_ERROR(x) ((long)(x) < 0)
