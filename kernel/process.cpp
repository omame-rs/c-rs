#include "process.h"
#include "log.h"
#include "vfs.h"
#include <cstdint>
#include <cstdlib>
#ifdef __EMSCRIPTEN__
#include <emscripten.h>
#endif

#define READ_CHUNK 1024

int next_pid = 1;

void spawn_js_process(const char *executable_path, int argc,
                      const char **argv) {
  // load JS data
  uint8_t *buf = (uint8_t *)malloc(READ_CHUNK);
  size_t fileSize = 0;
  size_t read = 0;
  VFSFile *file = vfs_open(executable_path, OPEN_R);
  while ((read = file->read(buf + fileSize, READ_CHUNK, fileSize)) ==
         READ_CHUNK) {
    fileSize += read;
    buf = (uint8_t *)realloc(buf, fileSize);
  }
  fileSize += read;

  printk("spawning PID %d (%s)", next_pid, executable_path);
  EM_ASM(
      {
        const data = HEAPU8.slice($0, $0 + $1);
        const blob = new Blob([data], {
          type:
            "application/javascript"
        });
        const script = document.createElement('script');
        script.src = URL.createObjectURL(blob);
        script.setAttribute('data-pid', $2);
        document.body.appendChild(script);
        document.body.removeChild(script);
        URL.revokeObjectURL(blob);
      },
      buf, fileSize, next_pid++);

  free(buf);
}
