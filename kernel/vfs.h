#pragma once
#include <cstddef>
#include <cstdint>
#include <map>
#include <vector>

class VFSFile {
public:
  virtual size_t read(uint8_t *buf, size_t size, size_t offset);
  virtual int write(uint8_t *buf, size_t size, size_t offset);
};

#define OPEN_R 1 << 0
#define OPEN_W 1 << 1
#define OPEN_CREATE 1 << 2

class VFSFileSystem {
public:
  virtual VFSFile *open(const char *path, int flags);
  virtual std::vector<char *> *readdir(const char *path);
  virtual int mkdir(const char *path);
  virtual int remove(const char *path);
};

class VFSDriver {
public:
  virtual const char *name();
  virtual VFSFileSystem *mount(const char *device);
};

void vfs_register_driver(VFSDriver *driver);
char vfs_mount(const char *device, const char *filesystem);
std::map<char, VFSFileSystem *> vfs_get_mounts();

VFSFile *vfs_open(const char *path, int flags);
std::vector<char *> *vfs_readdir(const char *path);
int vfs_mkdir(const char *path);
int vfs_remove(const char *path);
