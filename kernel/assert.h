#pragma once
#ifdef ENABLE_ASSERTS
#include "error.h"
#include "panic.h"

#define __STR_IMPL(x) #x
#define __STR(x) __STR_IMPL(x)

#define ASSERT(expression)                                                     \
  if (!(expression))                                                           \
    panic(-ERR_ASSERT, "Assertion failed: " #expression " (" __FILE__          \
                       ":" __STR(__LINE__) ")");

#else
#define ASSERT(expression)
#endif
