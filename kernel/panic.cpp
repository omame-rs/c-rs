#include "panic.h"
#include "drivers/fb/fbdriver.h"
#include "drivers/fb/fbmanager.h"
#include "graphics.h"
#include "log.h"
#include "rsrc/Inter-Regular.ttf.h"
#include <cstdio>
#include <emscripten.h>

bool has_panicked = false;

void showDeathScreen(int error_code, const char *reason);

void panic(int error_code, const char *reason) {
  if (has_panicked) {
    printk("panic called inside or after a panic");
    printk("error code: %#8x; reason: %s", error_code, reason);
    return; // we don't wanna panic inside or after a panic
  }

  has_panicked = true;
  printk("/!\\ PANIC");
  printk("error code: %#8x; reason: %s", error_code, reason);

  showDeathScreen(error_code, reason);

  emscripten_force_exit(-1);
}

void showDeathScreen(int error_code, const char *reason) {
  FBDriver *driver = fb_get_device(0);
  FBSize fbSize = driver->get_size();

  LibGraphics::Rectangle background({fbSize.width, fbSize.height},
                                    {0, 0, 0, 255});
  LibGraphics::LayeredObject screen(&background);

  LibGraphics::Font *font = LibGraphics::Font::loadFromMemory(
      (uint8_t *)rsrc_Inter_Regular_ttf, rsrc_Inter_Regular_ttf_size);

  int penX = 64;
  int penY = 64;
  font->setPixelSize(100);
  LibGraphics::Text emoticon(":(", font);
  screen.addChild(&emoticon, penX, penY);
  penY += emoticon.getSize().height;

  penY += 24;
  font->setPixelSize(24);
  LibGraphics::Text needsRestart(
      "RealSystem has panicked and needs to restart.", font);
  screen.addChild(&needsRestart, penX, penY);
  penY += needsRestart.getSize().height;

  char buf[1024];
  snprintf(buf, 1024, "Error code: %#x (%s)", error_code, reason);

  penY += 48;
  font->setPixelSize(14);
  LibGraphics::Text debugReport("When reporting this to the maintainers, "
                                "please supply the following information.",
                                font);
  screen.addChild(&debugReport, penX, penY);
  penY += debugReport.getSize().height;

  penY += 8;
  LibGraphics::Text debug(buf, font);
  screen.addChild(&debug, penX, penY);
  penY += debug.getSize().height;

  LibGraphics::objectToFB(&screen, driver, 0, 0);
}
