#pragma once
#include <functional>

typedef struct {
  std::function<void(char)> putchar;
} tty_device_funcs;

int tty_add_device(tty_device_funcs *tty);
tty_device_funcs *tty_get_device(int index);
int tty_get_device_count();

void tty_puts(tty_device_funcs *tty, char *s);
