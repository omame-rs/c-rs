#pragma once
#include "drivers/fb/fbdriver.h"
#include <psf.h>

class FBTTY {
public:
  FBTTY(FBDriver *device);

private:
  FBDriver *device = nullptr;
  int pos_x = 0;
  int pos_y = 0;
};

extern LibPSF::Font *psf_default;
