#include "drivers/tty/ttymanager.h"
#include <cstddef>
#include <cstdio>
#include <cstring>
#include <vector>

std::vector<tty_device_funcs *> devices;

int tty_add_device(tty_device_funcs *tty) {
  devices.push_back(tty);
  return devices.size() - 1;
}
tty_device_funcs *tty_get_device(int index) {
  if (index >= devices.size())
    return nullptr;
  return devices[index];
}
int tty_get_device_count() { return devices.size(); }

void tty_puts(tty_device_funcs *tty, char *s) {
  if (tty == nullptr)
    return;
  size_t len = strlen(s);
  for (size_t i = 0; i < len; i++) {
    tty->putchar(s[i]);
  }
  tty->putchar('\n');
}
