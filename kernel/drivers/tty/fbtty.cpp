#include "drivers/tty/fbtty.h"
#include "drivers/tty/ttymanager.h"
#include <cstdlib>
#include <cstring>
#include <psf.h>

FBTTY::FBTTY(FBDriver *device) : device(device) {
  tty_device_funcs *funcs =
      (tty_device_funcs *)malloc(sizeof(tty_device_funcs));
  funcs->putchar = [this](char c) {
    if (!this->device->is_ready)
      return;
    int real_x = pos_x * psf_default->width;
    int real_y = pos_y * psf_default->height;
    if (c != '\n') {
      uint8_t *data =
          psf_default->glyph_to_rgba(c, {0, 0, 0, 255}, {255, 255, 255, 255});

      this->device->write(data, real_x, real_y, psf_default->width,
                          psf_default->height);
      free(data);
      pos_x++;
      // if pos_x is going off-screen, wrap around
      if (pos_x * psf_default->width >= this->device->get_size().width) {
        pos_y++;
        pos_x = 0;
      }
    } else {
      pos_y++;
      pos_x = 0;
    }
    // if pos_y is going off-screen, scroll fb
    if (pos_y * psf_default->height >= this->device->get_size().height) {
      pos_y--;
      auto size = this->device->get_size();
      this->device->copy(size.width, size.height, 0, 0, 0,
                         -psf_default->height);

      // clear the black area
      int wrap_black_size = size.width * psf_default->height * 4;
      uint8_t *wrap_black = (uint8_t *)malloc(wrap_black_size);
      memset(wrap_black, 0, wrap_black_size);
      for (int i = 0; i < wrap_black_size; i += 4) {
        wrap_black[i + 3] = 255; // alpha
      }
      this->device->write(wrap_black, 0, size.height - psf_default->height,
                          size.width, psf_default->height);
      free(wrap_black);
    }
  };
  tty_add_device(funcs);
}
