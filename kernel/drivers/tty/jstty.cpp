#include "drivers/tty/ttymanager.h"
#include <cstdio>

void jstty_putchar(char c) { putchar(c); }

tty_device_funcs funcs = {
    .putchar = &jstty_putchar,
};

void jstty_register() {
  puts("jstty registered");
  tty_add_device(&funcs);
}
