#pragma once
#include "drivers/fb/fbdriver.h"

int fb_add_device(FBDriver *driver);
FBDriver *fb_get_device(int index);
int fb_get_device_count();
