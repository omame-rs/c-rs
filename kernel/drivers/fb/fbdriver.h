#pragma once
#include <cstddef>
#include <cstdint>

typedef struct {
  int width;
  int height;
} FBSize;

class FBDriver {
public:
  FBDriver();

  virtual void write(uint8_t *data, int x, int y, int w, int h);
  virtual void resize(const FBSize &size);
  virtual void copy(int w, int h, int old_x, int old_y, int x, int y);
  virtual const FBSize get_size();

  bool is_ready = false;
};
