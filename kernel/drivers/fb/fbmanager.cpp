#include "drivers/fb/fbmanager.h"
#include "drivers/fb/fbdriver.h"
#include "log.h"
#include <vector>

std::vector<FBDriver *> fbs;

int fb_add_device(FBDriver *driver) {
  fbs.push_back(driver);
  printk("FBManager: added fb device %p", driver);
  // TODO: create TTY device too
  return fbs.size() - 1;
}

FBDriver *fb_get_device(int index) { return fbs[index]; }
