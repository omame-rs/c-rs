#pragma once
#include "drivers/fb/fbdriver.h"

class CanvasFBDriver : public FBDriver {
public:
  CanvasFBDriver();

  void write(uint8_t *data, int x, int y, int w, int h);
  void resize(const FBSize &size);
  void copy(int w, int h, int old_x, int old_y, int x, int y);
  const FBSize get_size();
};
