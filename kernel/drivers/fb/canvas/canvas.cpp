#include "canvas.h"
#include "drivers/fb/fbdriver.h"
#include "log.h"
#include <emscripten.h>

CanvasFBDriver::CanvasFBDriver() : FBDriver() {
  printk("CanvasFBDriver: init");
  MAIN_THREAD_EM_ASM({
    const canvas = document.querySelector("#canvas");
    const ctx = canvas.getContext("2d");
    Module._canvas = new Object();
    Module._canvas.canvas = canvas;
    Module._canvas.ctx = ctx;
    // as a part of init, fill the canvas with black
    ctx.fillStyle = "black";
    ctx.fillRect(0, 0, canvas.width, canvas.height);
  });
  this->is_ready = true;
}

void CanvasFBDriver::write(uint8_t *data, int x, int y, int w, int h) {
  if (!is_ready)
    return;
  MAIN_THREAD_EM_ASM(
      {
        // args
        const data = $0;
        const x = $1;
        const y = $2;
        const w = $3;
        const h = $4;

        const ctx = Module._canvas.ctx;
        const imageData = ctx.createImageData(w, h);
        imageData.data.set(HEAPU8.slice(data, data + (w * h * 4)));
        ctx.putImageData(imageData, x, y);
      },
      data, x, y, w, h);
}

void CanvasFBDriver::resize(const FBSize &size) {
  if (!is_ready)
    return;
  printk("CanvasFBDriver: resizing canvas to %dx%d", size.width, size.height);
  MAIN_THREAD_EM_ASM(
      {
        Module._canvas.canvas.width = $0;
        Module._canvas.canvas.height = $1;
      },
      size.width, size.height);
}

const FBSize CanvasFBDriver::get_size() {
  if (!is_ready)
    return {-1, -1};
  int width = MAIN_THREAD_EM_ASM_INT({ return Module._canvas.canvas.width; });
  int height = MAIN_THREAD_EM_ASM_INT({ return Module._canvas.canvas.height; });
  return {width, height};
}

void CanvasFBDriver::copy(int w, int h, int old_x, int old_y, int x, int y) {
  if (!is_ready)
    return;
  MAIN_THREAD_EM_ASM(
      {
        // args
        const w = $0;
        const h = $1;
        const old_x = $2;
        const old_y = $3;
        const x = $4;
        const y = $5;

        const ctx = Module._canvas.ctx;
        const imageData = ctx.getImageData(old_x, old_y, w, h);
        ctx.putImageData(imageData, x, y);
      },
      w, h, old_x, old_y, x, y);
}
