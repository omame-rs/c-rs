#include "fbdriver.h"
#include "drivers/fb/fbmanager.h"
#include "drivers/tty/fbtty.h"

FBDriver::FBDriver() {
  fb_add_device(this);
  // init fbtty
  new FBTTY(this);
}

void FBDriver::write(uint8_t *data, int x, int y, int w, int h) {}
void FBDriver::resize(const FBSize &size) {}
void FBDriver::copy(int w, int h, int old_x, int old_y, int x, int y) {}
const FBSize FBDriver::get_size() { return {-1, -1}; }
