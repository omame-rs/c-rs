#pragma once
#include "vfs.h"
#include <map>
#include <pthread.h>
#include <vector>

class TMPFSFile : public VFSFile {
public:
  TMPFSFile();

  size_t read(uint8_t *buf, size_t size, size_t offset);
  int write(uint8_t *buf, size_t size, size_t offset);

private:
  std::vector<uint8_t> m_data;
  pthread_mutex_t m_mutex;
};

class TMPFSFolder {
public:
  TMPFSFolder();
  std::map<char *, TMPFSFile *> files;
  std::map<char *, TMPFSFolder *> folders;
};

class TMPFSFileSystem : public VFSFileSystem {
public:
  TMPFSFileSystem();

  TMPFSFile *open(const char *path, int flags);
  // std::vector<char *> readdir(const char *path);
  int mkdir(const char *path);
  // void remove(const char *path);

private:
  TMPFSFolder m_root;
  pthread_mutex_t m_mutex;
};

class TMPFSDriver : public VFSDriver {
public:
  TMPFSDriver();

  const char *name();
  TMPFSFileSystem *mount(const char *device);
};
