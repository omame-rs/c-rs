#pragma once
#include "assert.h"
#include "vfs.h"
#define assert(x) ASSERT(x)
#include <nlohmann/json.hpp>

class HTTPFSFile : public VFSFile {
public:
  HTTPFSFile(const char *fetch_path);

  size_t read(uint8_t *buf, size_t size, size_t offset);

private:
  const char *m_fetch_path;
};

class HTTPFSFileSystem : public VFSFileSystem {
public:
  HTTPFSFileSystem(nlohmann::json json);

  HTTPFSFile *open(const char *path, int flags);

private:
  nlohmann::json m_json;
};

class HTTPFSDriver : public VFSDriver {
public:
  HTTPFSDriver();

  const char *name();
  HTTPFSFileSystem *mount(const char *device);
};
