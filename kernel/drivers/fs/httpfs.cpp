#include "httpfs.h"
#include "error.h"
#include "nlohmann/json.hpp"
#include "vfs.h"
#include <cstdlib>
#include <cstring>
#ifdef __EMSCRIPTEN__
#include <emscripten.h>
#endif

// File
HTTPFSFile::HTTPFSFile(const char *fetch_path) {
  size_t len = strlen(fetch_path);
  m_fetch_path = (const char *)malloc(len + 1);
  memcpy((void *)m_fetch_path, fetch_path, len + 1);
}

EM_ASYNC_JS(size_t, httpfs_file_read,
            (const char *url, size_t urlLen, uint8_t *buf, size_t size,
             size_t offset),
            {
              if (!Module.httpfsCache)
                Module.httpfsCache = {};

              const url_decoded =
                  new TextDecoder().decode(HEAPU8.slice(url, url + urlLen));

              if (!Module.httpfsCache[url_decoded]) {
                const resp = await fetch(url_decoded);
                const data = await resp.arrayBuffer();
                Module.httpfsCache[url_decoded] = new Uint8Array(data);
              }
              const data = Module.httpfsCache[url_decoded];
              const dataSlice = data.slice(offset, offset + size);
              HEAPU8.set(dataSlice, buf);
              return dataSlice.length;
            });

size_t HTTPFSFile::read(uint8_t *buf, size_t size, size_t offset) {
  return httpfs_file_read(m_fetch_path, strlen(m_fetch_path), buf, size,
                          offset);
}

// FileSystem
HTTPFSFileSystem::HTTPFSFileSystem(nlohmann::json json) : m_json(json) {}

HTTPFSFile *HTTPFSFileSystem::open(const char *path, int flags) {
  if (flags != OPEN_R) {
    return (HTTPFSFile *)-ERR_INVALID;
  }
  // traverse the path by slashes until we reach \0
  const char *prevDir = path + 1;
  nlohmann::json currentRoot = m_json;

  while (*(++path) != 0) {
    if (*path == '/') {
      int folderLen = path - prevDir;
      char *folderName = (char *)malloc(folderLen + 1);
      memcpy(folderName, prevDir, folderLen);
      folderName[folderLen] = 0;

      // this will probably not work but whatever
      if (!currentRoot.contains(folderName)) {
        free(folderName);
        return (HTTPFSFile *)-ERR_NOT_EXIST;
      }

      currentRoot = currentRoot[folderName];
      free(folderName);
      prevDir = path + 1;
    }
  }

  // final iter
  {
    int folderLen = path - prevDir;
    char *folderName = (char *)malloc(folderLen + 1);
    memcpy(folderName, prevDir, folderLen);
    folderName[folderLen] = 0;

    // this will probably not work but whatever
    if (!currentRoot.contains(folderName)) {
      free(folderName);
      return (HTTPFSFile *)-ERR_NOT_EXIST;
    }

    currentRoot = currentRoot[folderName];
    free(folderName);
  }

  if (!currentRoot.is_string()) {
    return (HTTPFSFile *)-ERR_INVALID;
  }

  std::string url = currentRoot.get<std::string>();

  return new HTTPFSFile(url.c_str());
}

// Driver
HTTPFSDriver::HTTPFSDriver() { vfs_register_driver(this); }

EM_ASYNC_JS(char *, httpfs_do_fetch, (const char *url, size_t len), {
  const url_decoded = new TextDecoder().decode(HEAPU8.slice(url, url + len));
  const resp = await fetch(url_decoded);
  const text = await resp.text();
  const data = new TextEncoder().encode(text);
  const ptr = _malloc(data.length + 1);
  HEAPU8.set(data, ptr);
  HEAPU8[ptr + data.length] = 0;
  return ptr;
});

const char *HTTPFSDriver::name() { return "httpfs"; }

HTTPFSFileSystem *HTTPFSDriver::mount(const char *device) {
  // fetch JSON
  char *json_raw = httpfs_do_fetch(device, strlen(device));
  // decode it
  nlohmann::json json = nlohmann::json::parse(json_raw);
  // init fs
  return new HTTPFSFileSystem(json);
}
