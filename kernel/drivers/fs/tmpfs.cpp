#include "drivers/fs/tmpfs.h"
#include "error.h"
#include "flags.h"
#include "log.h"
#include "vfs.h"
#include <algorithm>
#include <cstring>
#include <pthread.h>
#include <utility>

// driver
TMPFSDriver::TMPFSDriver() { vfs_register_driver(this); }
const char *TMPFSDriver::name() { return "tmpfs"; }

TMPFSFileSystem *TMPFSDriver::mount(const char *device) {
  printk("tmpfs: creating new tmpfs");
  return new TMPFSFileSystem();
}

// filesystem
TMPFSFileSystem::TMPFSFileSystem() { pthread_mutex_init(&m_mutex, NULL); }

TMPFSFile *TMPFSFileSystem::open(const char *path, int flags) {
  pthread_mutex_lock(&m_mutex);
  TMPFSFile *file = nullptr;

  // traverse the path by slashes until we reach \0
  const char *prevDir = path + 1;
  TMPFSFolder *currentFolder = &m_root;

  int fileLen;
  char *fileName;

  while (*(++path) != '\0') {
    if (*path == '/') {
      int folderLen = path - prevDir;
      char *folderName = (char *)malloc(folderLen + 1);
      memcpy(folderName, prevDir, folderLen);
      folderName[folderLen] = 0;

      TMPFSFolder *newFolder = nullptr;
      // for each dir name, check if it has a Folder entry in this
      for (const auto &[name, folder] : currentFolder->folders) {
        if (strncmp(name, folderName, folderLen) == 0) {
          newFolder = folder;
          break;
        }
      }
      // if there isn't, goto end
      if (newFolder == nullptr) {
        file = (TMPFSFile *)-ERR_NOT_EXIST;
        free(folderName);
        goto end;
      }

      currentFolder = newFolder;
      prevDir = path + 1;

      free(folderName);
    }
  }

  // now we should have the filename in prevDir
  fileLen = path - prevDir;
  for (const auto &[name, checkedFile] : currentFolder->files) {
    if (strncmp(name, prevDir, fileLen) == 0) {
      file = checkedFile;
      goto end;
    }
  }

  if (file == nullptr && HAS_FLAG(flags, OPEN_CREATE)) {
    file = new TMPFSFile();
    fileName = (char *)malloc(fileLen + 1);
    memcpy(fileName, prevDir, fileLen);
    fileName[fileLen] = 0;
    currentFolder->files[fileName] = file;
    goto end;
  }

  file = (TMPFSFile *)-ERR_NOT_EXIST;

end:
  pthread_mutex_unlock(&m_mutex);
  return file;
}

int TMPFSFileSystem::mkdir(const char *path) {
  pthread_mutex_lock(&m_mutex);
  int err = 0;

  // traverse the path by slashes until we reach \0
  const char *prevDir = path + 1;
  TMPFSFolder *currentFolder = &m_root;
  TMPFSFolder *folder = nullptr;

  int fileLen;
  char *fileName;

  while (*(++path) != '\0') {
    if (*path == '/') {
      int folderLen = path - prevDir;
      char *folderName = (char *)malloc(folderLen + 1);
      memcpy(folderName, prevDir, folderLen);
      folderName[folderLen] = 0;

      TMPFSFolder *newFolder = nullptr;
      // for each dir name, check if it has a Folder entry in this
      for (const auto &[name, folder] : currentFolder->folders) {
        if (strncpy(name, folderName, folderLen) == 0) {
          newFolder = folder;
          break;
        }
      }
      // if there isn't, goto end
      if (newFolder == nullptr) {
        err = -ERR_NOT_EXIST;
        goto end;
      }

      currentFolder = newFolder;
      prevDir = path + 1;

      free(folderName);
    }
  }

  // now we should have the filename in prevDir
  fileLen = path - prevDir;
  folder = new TMPFSFolder();
  fileName = (char *)malloc(fileLen + 1);
  memcpy(fileName, prevDir, fileLen);
  fileName[fileLen] = 0;
  currentFolder->folders[fileName] = folder;

end:
  pthread_mutex_unlock(&m_mutex);
  return err;
}

// folder
TMPFSFolder::TMPFSFolder() {}

// file
TMPFSFile::TMPFSFile() { pthread_mutex_init(&m_mutex, NULL); }

size_t TMPFSFile::read(uint8_t *buf, size_t size, size_t offset) {
  pthread_mutex_lock(&m_mutex);
  size_t bytes_read = 0;
  for (size_t i = offset; i < m_data.size(); i++) {
    buf[bytes_read] = m_data[i];
    bytes_read++;
  }
  pthread_mutex_unlock(&m_mutex);
  return bytes_read;
}

int TMPFSFile::write(uint8_t *buf, size_t size, size_t offset) {
  pthread_mutex_lock(&m_mutex);
  if (size + offset > m_data.size()) {
    m_data.resize(size + offset);
  }
  for (size_t i = 0; i < size; i++) {
    m_data[i + offset] = buf[i];
  }
  pthread_mutex_unlock(&m_mutex);
  return 0;
}
