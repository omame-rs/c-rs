#include "drivers/timer/manager.h"
#include "log.h"

timer_funcs stub_timer = {
    .get_abstime = []() -> double { return 0.; },
    .get_reltime = []() -> double { return 0.; },
};

timer_funcs *m_timer = &stub_timer;

void timer_register(timer_funcs *timer) {
  printk("TimerManager: new timer registered %p", timer);
  m_timer = timer;
}
timer_funcs *timer_get() { return m_timer; }
