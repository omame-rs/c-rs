#include "drivers/timer/jstimer.h"
#include "drivers/timer/manager.h"
#include <cstdio>
#include <emscripten.h>

double reltime_offset = 0;

double js_get_abstime() {
  return EM_ASM_DOUBLE(
      {return performance.timeOrigin + performance.now() / 1000});
}
double js_get_reltime() {
  return EM_ASM_DOUBLE({return performance.now() / 1000}) - reltime_offset;
}

timer_funcs jstimer = {
    .get_abstime = &js_get_abstime,
    .get_reltime = &js_get_reltime,
};

void jstimer_register() {
  reltime_offset = js_get_reltime();
  timer_register(&jstimer);
}
