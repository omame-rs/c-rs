#pragma once

typedef struct {
  double (*get_abstime)();
  double (*get_reltime)();
} timer_funcs;

void timer_register(timer_funcs *timer);
timer_funcs *timer_get();
