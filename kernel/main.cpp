#include "cmdline.h"
#include "drivers/fb/canvas/canvas.h"
#include "drivers/fs/httpfs.h"
#include "drivers/fs/tmpfs.h"
#include "drivers/timer/jstimer.h"
#include "drivers/tty/jstty.h"
#include "event/loop.h"
#include "graphics.h"
#include "graphics_init.h"
#include "log.h"
#include "process.h"
#include "psf.h"
#include "rsrc/Inter-Regular.ttf.h"
#include "rsrc/Tamsyn8x16r.psf.h"
#include "theme.h"
#include "version.h"
#include "vfs.h"
#include <cstdlib>
#include <cstring>
#ifdef __EMSCRIPTEN__
#include <emscripten.h>
#endif

LibPSF::Font *psf_default;
CanvasFBDriver *driver;

int main() {
  load_cmdline();
  jstty_register();
  jstimer_register();
  psf_default = new LibPSF::Font((uint8_t *)rsrc_Tamsyn8x16r_psf);
  driver = new CanvasFBDriver();

  printk("hello, world! c-rs kernel build %s, compiled on %s",
         KERNEL_BUILD_STRING, __DATE__ " " __TIME__);
  event_loop_register();

  new TMPFSDriver();
  new HTTPFSDriver();

  LibGraphics::Internal::init();
  printk("making sure LibTheme is usable...");
  LibTheme::setFont(
      LibTheme::ThemeFont::TH_TITLE_BAR_FONT,
      LibGraphics::Font::loadFromMemory((uint8_t *)rsrc_Inter_Regular_ttf,
                                        rsrc_Inter_Regular_ttf_size));

  // assume root is httpfs since i'm lazy
  // TODO: handle different filesytem roots properly!!
  printk("mounting root...");
  char mounted = vfs_mount(get_cmdline_value("root") + 7, "httpfs");

  spawn_js_process("A:/RealSystem/rslogon.js", 0, NULL);

  return 0;
}
