cmake_minimum_required(VERSION 3.24)

execute_process(
    COMMAND git rev-list --count HEAD
    OUTPUT_VARIABLE GIT_COMMIT_COUNT
    OUTPUT_STRIP_TRAILING_WHITESPACE
)
configure_file(version.h.in ${CMAKE_CURRENT_BINARY_DIR}/generated/version.h)

add_executable(kernel
    drivers/fb/canvas/canvas.cpp
    drivers/fb/fbdriver.cpp
    drivers/fb/fbmanager.cpp

    drivers/fs/httpfs.cpp
    drivers/fs/tmpfs.cpp

    drivers/timer/jstimer.cpp
    drivers/timer/manager.cpp

    drivers/tty/fbtty.cpp
    drivers/tty/jstty.cpp
    drivers/tty/ttymanager.cpp

    event/loop.cpp

    cmdline.cpp
    log.cpp
    main.cpp
    panic.cpp
    process.cpp
    vfs.cpp
)

rs_add_resource(kernel rsrc/Tamsyn8x16r.psf)
rs_add_resource(kernel rsrc/Inter-Regular.ttf)

target_link_options(kernel
    PRIVATE
    -sEXPORTED_RUNTIME_METHODS=[ccall,cwrap]
    -sEXPORTED_FUNCTIONS=[${EXPORTED_FUNCTIONS}]
)

target_include_directories(kernel
    PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}
    PRIVATE ${CMAKE_CURRENT_BINARY_DIR}/generated
)

target_link_libraries(kernel
    libgraphics
    libpsf
    libjs
    libwindow
    libtheme

    nlohmann_json
)

set(KERNEL_BINARY_DIR "${CMAKE_CURRENT_BINARY_DIR}" CACHE STRING "Kernel's binary directory" FORCE)
