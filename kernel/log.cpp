#include "log.h"
#include "cmdline.h"
#include "drivers/timer/manager.h"
#include "drivers/tty/ttymanager.h"
#include <cstdarg>
#include <cstdio>
#include <string>

void printk(const char *format, ...) {
  char buffer[256];
  va_list args;
  va_start(args, format);
  vsnprintf(buffer, 256, format, args);
  va_end(args);

  char final_buffer[384];
  snprintf(final_buffer, 384, "[%f] %s", timer_get()->get_reltime(), buffer);

  // by default, we log to all TTYs unless specified otherwise
  // if the desired TTY is unavailable, we log to all TTYs until it is
  int desiredTTY = -1;
  char *desiredTTYString = get_cmdline_value("console");
  if (desiredTTYString != nullptr) {
    desiredTTY = std::atoi(desiredTTYString);
    if (tty_get_device(desiredTTY) == nullptr)
      desiredTTY = -1;
  }

  if (desiredTTY == -1) {
    for (int i = 0; i < tty_get_device_count(); i++) {
      tty_puts(tty_get_device(i), final_buffer);
    }
  } else {
    tty_puts(tty_get_device(desiredTTY), final_buffer);
  }
}
