#include "vfs.h"
#include "error.h"
#include "log.h"
#include <algorithm>
#include <cstring>
#include <pthread.h>
#include <vector>

std::vector<VFSDriver *> drivers;
std::map<char, VFSFileSystem *> mounts;
pthread_mutex_t mounts_mutex = PTHREAD_MUTEX_INITIALIZER;

// main VFS functions
void vfs_register_driver(VFSDriver *driver) {
  drivers.push_back(driver);
  printk("VFS: registered new driver %s", driver->name());
}
char vfs_mount(const char *device, const char *filesystem) {
  VFSDriver *driver;
  VFSFileSystem *fs;

  pthread_mutex_lock(&mounts_mutex);
  // find available char
  char mount = 'A' - 1;

  while (mounts.find(++mount) != mounts.end())
    ;

  // find VFS driver
  auto is_driver = [filesystem](VFSDriver *driver) {
    return strcmp(driver->name(), filesystem) == 0;
  };
  auto it = std::find_if(drivers.begin(), drivers.end(), is_driver);
  if (it == drivers.end()) {
    mount = -1;
    goto end;
  }
  driver = *it;

  // mount fs
  fs = driver->mount(device);
  mounts[mount] = fs;
  printk("VFS: mounted device %s (%s) at %c:", device, filesystem, mount);

end:
  pthread_mutex_unlock(&mounts_mutex);
  return mount;
}

std::map<char, VFSFileSystem *> vfs_get_mounts() {
  pthread_mutex_lock(&mounts_mutex);
  std::map<char, VFSFileSystem *> mounts_copy = mounts;
  pthread_mutex_unlock(&mounts_mutex);
  return mounts_copy;
}

VFSFileSystem *_vfs_get_fs(const char *path) {
  VFSFileSystem *fs = nullptr;
  pthread_mutex_lock(&mounts_mutex);

  char mount = path[0];
  auto it = mounts.find(mount);
  if (it == mounts.end())
    goto end;
  fs = it->second;

end:
  pthread_mutex_unlock(&mounts_mutex);
  return fs;
}

#define DEFINE_VFS_WRAPPER_WITH_ARG(type, name, arg, retval)                   \
  type name(const char *path, arg) {                                           \
    VFSFileSystem *fs = _vfs_get_fs(path);                                     \
    if (fs == nullptr)                                                         \
      return (type)-ERR_INVALID;                                               \
    return retval;                                                             \
  }

#define DEFINE_VFS_WRAPPER(type, name, retval)                                 \
  type name(const char *path) {                                                \
    VFSFileSystem *fs = _vfs_get_fs(path);                                     \
    if (fs == nullptr)                                                         \
      return (type)-ERR_INVALID;                                               \
    return retval;                                                             \
  }

DEFINE_VFS_WRAPPER_WITH_ARG(VFSFile *, vfs_open, int flags,
                            fs->open(path + 2, flags))
DEFINE_VFS_WRAPPER(std::vector<char *> *, vfs_readdir, fs->readdir(path + 2))
DEFINE_VFS_WRAPPER(int, vfs_mkdir, fs->mkdir(path + 2))
DEFINE_VFS_WRAPPER(int, vfs_remove, fs->remove(path + 2))

// stub class functions
size_t VFSFile::read(uint8_t *buf, size_t size, size_t offset) {
  return -ERR_NOT_IMPL;
}
int VFSFile::write(uint8_t *buf, size_t size, size_t offset) {
  return -ERR_NOT_IMPL;
}

VFSFile *VFSFileSystem::open(const char *path, int flags) {
  return (VFSFile *)-ERR_NOT_IMPL;
}
std::vector<char *> *VFSFileSystem::readdir(const char *path) {
  return (std::vector<char *> *)-ERR_NOT_IMPL;
}
int VFSFileSystem::mkdir(const char *path) { return -ERR_NOT_IMPL; }
int VFSFileSystem::remove(const char *path) { return -ERR_NOT_IMPL; }

const char *VFSDriver::name() { return "(stub)"; }
VFSFileSystem *VFSDriver::mount(const char *device) {
  return (VFSFileSystem *)-ERR_NOT_IMPL;
}
