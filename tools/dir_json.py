#!/usr/bin/env python3

import sys
import os
import json

http_path = sys.argv[1]
local_path = sys.argv[2]

root = {}

print(f"Generating HTTPFS index of {local_path} (served at {http_path})")

for dirpath, dirnames, filenames in os.walk(local_path):
    dirpath = dirpath[len(local_path) :]
    if dirpath == "":
        dirpath = "/"

    # traverse dirpath
    our_root = root
    if dirpath != "/":
        for folder in dirpath.split("/")[1:]:
            our_root = our_root[folder]

    # populate our_root with the folders and files
    for dirname in dirnames:
        our_root[dirname] = {}

    for filename in filenames:
        if not dirpath.endswith("/"):
            dirpath = dirpath + "/"
        our_root[filename] = http_path + dirpath + filename

with open(f"{local_path}/drive.json", "w+") as file:
    json.dump(root, file, separators=(",", ":"))
