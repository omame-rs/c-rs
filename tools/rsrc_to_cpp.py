#!/usr/bin/env python3

import sys
import os

path = sys.argv[1]

source = sys.argv[2]
dest = sys.argv[3]
dest_h = dest.replace(".cpp", ".h")

os.makedirs(os.path.dirname(dest), exist_ok=True)

with open(source, "rb") as file:
    source_data = file.read()

source_name = (
    source[len(path) + 1 :]
    .replace("/", "_")
    .replace("\\", "_")
    .replace(".", "_")
    .replace("-", "_")
)

source_cpp = f"""#include "{dest_h}"

const uint8_t {source_name}[] = {{
  {','.join([str(i) for i in source_data])}
}};
"""

source_h = f"""#pragma once
#include <cstdint>

#define {source_name}_size {len(source_data)}

extern const uint8_t {source_name}[];
"""

with open(dest, "w+") as file:
    file.write(source_cpp)

with open(dest_h, "w+") as file:
    file.write(source_h)
