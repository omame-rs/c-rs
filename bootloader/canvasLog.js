const /** @type HTMLCanvasElement */ canvas = document.getElementById("canvas");
let /** @type CanvasRenderingContext2D | null */ ctx = null;
let penY = 0;

const TEXT_SIZE = 14;

export function init() {
  ctx = canvas.getContext("2d");
  ctx.fillStyle = "black";
  ctx.fillRect(0, 0, canvas.width, canvas.height);
  ctx.fillStyle = "white";
  ctx.font = `${TEXT_SIZE}px monospace`;
  penY = TEXT_SIZE;
}

export function exit() {
  ctx = null;
}

export function print(/** @type string */ str) {
  if (ctx == null) throw new Error("canvasLog not initialized");
  ctx.fillText(str, 0, penY);
  penY += TEXT_SIZE;

  console.log("BOOTLDR:", str);
}
