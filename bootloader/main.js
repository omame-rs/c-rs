import { init, print } from "./canvasLog.js";
import { HTTPFS } from "./httpfs.js";

const VERSION = "0.0.1";

init();
print(`RealSystem bootloader v${VERSION}`);

const CONFIG_DEFAULTS = {
  version: 1,
  boot: {
    items: {
      RSINST: {
        title: "RealSystem Installer",
        drive: "httpfs:/boot/drive.json",
        kernel: "/kernel.js",
        append: "root=httpfs:/installer/drive.json",
      },
    },
    order: ["RSINST"],
  },
};

// check for bootloader configuration
if (!("bootloader_data" in localStorage)) {
  print("Bootloader data missing, restoring defaults...");
  localStorage.bootloader_data = JSON.stringify(CONFIG_DEFAULTS);
}

const /** @type typeof CONFIG_DEFAULTS */ config = JSON.parse(
    localStorage.bootloader_data
  );

if (!("version" in config)) {
  print("WARN: Version is missing from configuration, assuming v1");
  config.version = 1;
  localStorage.bootloader_data = JSON.stringify(config);
}

function fatalError(reason) {
  print(new Array(100).fill("-").join(""));
  print("");
  print("   FATAL ERROR:");
  print("");
  print(`   ${reason}`);
  print("   Cannot continue.");
  throw new Error(reason);
}

// check if we're in a secure context
if (!("SharedArrayBuffer" in globalThis)) {
  fatalError(
    "This browser and/or environment does not support secure contexts."
  );
}

const normalBoot = async () => {
  for (const item of config.boot.order) {
    const bootData = config.boot.items[item];
    print(`Booting ${bootData.title} (${item})...`);
    try {
      let fs = null;
      switch (bootData.drive.split(":")[0]) {
        case "httpfs": {
          fs = new HTTPFS(bootData.drive.slice(7));
          await fs.load();
          break;
        }
        default: {
          throw new Error(`Unknown filesystem ${bootData.drive.split(":")[0]}`);
        }
      }
      const url = fs.getFileURL(bootData.kernel);

      window.KERNEL_APPEND = bootData.append;

      const script = document.createElement("script");
      script.src = url;
      document.body.append(script);
      return;
    } catch (error) {
      print(`Booting ${item} failed: ${error}. Check JS console for details`);
      console.error(error);
    }
  }
};

let bootTimeout;
print("Press ESC in 3 seconds to interrupt boot...");
const escListener = (/** @type KeyboardEvent */ event) => {
  if (event.key === "Escape") {
    print("--- Settings menu (WIP)");
    print("Press C to clear bootloader data.");
    print("Press K to restart.");
    window.addEventListener("keydown", (event) => {
      if (event.key.toLowerCase() == "c") {
        delete localStorage.bootloader_data;
        print("Done, restarting...");
        window.location.reload();
      }
      if (event.key.toLowerCase() == "k") {
        print("Restarting...");
        window.location.reload();
      }
    });

    window.removeEventListener("keydown", escListener);
    clearTimeout(bootTimeout);
  }
};
window.addEventListener("keydown", escListener);

bootTimeout = setTimeout(() => {
  window.removeEventListener("keydown", escListener);
  normalBoot();
}, 3 * 1000);
