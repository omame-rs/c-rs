export class HTTPFS {
  constructor(url) {
    this.url = url;
  }

  async load() {
    const resp = await fetch(this.url);
    this.data = await resp.json();
  }

  getFileURL(/** @type string */ path) {
    const segments = path.split("/").slice(1);
    let currentObject = this.data;
    for (const segment of segments) {
      if (!(segment in currentObject)) {
        throw new Error("File not found");
      }
      currentObject = currentObject[segment];
    }

    return currentObject;
  }
}
